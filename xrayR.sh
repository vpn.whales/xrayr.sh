
NODE_ID=$1
HOST=$2

DNS_IP=""
if [ -n "$3" ]; then
  IP=`ping ${DNS_HOST} -c 1 -w 1 | sed '1{s/[^(]*(//;s/).*//;q}'`
  if [ -n "${IP}" ]; then
    if [ $IP != "1.1.1.1" ]; then
      DNS_IP=$IP
    fi
  fi
fi

echo "NODE-ID:${NODE_ID}  HOST:${HOST} DNS:${DNS_IP}"

rm -rf /joy/XrayR
git clone https://github.com/XrayR-project/XrayR-release /joy/XrayR

rm -f /joy/XrayR/config/config.yml
rm -f /joy/XrayR/config/dns.json
rm -f /joy/XrayR/docker-compose.yml

curl https://gitlab.com/vpn.whales/xrayr.sh/-/raw/master/config.yml -o /joy/XrayR/config/config.yml
curl https://gitlab.com/vpn.whales/xrayr.sh/-/raw/master/dns.json -o /joy/XrayR/config/dns.json
curl https://gitlab.com/vpn.whales/xrayr.sh/-/raw/master/docker-compose.yml -o /joy/XrayR/docker-compose.yml
curl https://gitlab.com/vpn.whales/xrayr.sh/-/raw/master/fullback.nginx.conf -o /joy/XrayR/fullback.nginx.conf

yum install jq -y
apt install jq -y

echo "$(cat /joy/XrayR/config/config.yml | sed "s#1111#$NODE_ID#g")" > /joy/XrayR/config/config.yml
echo "$(cat /joy/XrayR/config/config.yml | sed "s#target#$HOST#g")" > /joy/XrayR/config/config.yml
echo "$(cat /joy/XrayR/fullback.nginx.conf | sed "s#main_host#$HOST#g")" > /joy/XrayR/fullback.nginx.conf

if [ -n "${DNS_IP}" ]; then
	echo "$(cat /joy/XrayR/config/dns.json | sed "s#dnsdomain#$DNS_IP#g")" > /joy/XrayR/config/dns.json
fi

cd /joy/XrayR/ && docker-compose up -d --force